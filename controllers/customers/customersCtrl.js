var customersModel = require('../../models/customers/customersModel');

exports.getName = async (request, response) => {
  idcliente = request.idcliente;
  try {
    var result = await customersModel.getName(idcliente);
  } catch (error) {
    return response.status(500).send().end(); // error del servidor
  }
  var fullName = result.body;
  var data = {};
  data.nombre = fullName[0].nombre;
  data.paterno = fullName[0].paterno;
  response.status(200).send(data).end();
};