var locationsModel = require('../../models/locations/locationsModel');

exports.getBranches = (request, response) => {
  var branches = locationsModel.getBranches();
  return response.status(200).send(branches).end();
};

exports.getATMs = (request, response) => {
  var ATMs = locationsModel.getATMs();
  return response.status(200).send(ATMs).end();
};

// exports.getBranches = async (request, response) => {
//   var result = locationsModel.branches();
//   // try {
//   //   var result = await locationsModel.branches();
//   // } catch (error) {
//   //   return response.status(500).send().end();
//   // }
//   // var locations = result;
//   return response.status(200).send().end();
//   // return response.status(200).send(locations).end();
// };
