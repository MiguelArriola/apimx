var helpers = require('../../helpers/helpers');
var accountsModel = require('../../models/accounts/accountsModel');
var movementsModel = require('../../models/movements/movementsModel');

exports.post = async (request, response) => {
  var transaction = request.body;
  var { nombre, tipo, origen, destino, importe } = transaction;
  amount = parseFloat(importe);
  /*  */
  /* REALIZA VALIDACIONES */
  /*  */
  // CUENTA ORIGEN
  try {
    var originResponse = await accountsModel.getBalance(origen);
  } catch (error) {
    return response.status(500).send().end(); // error del servidor
  }
  // ¿Existe Cuenta Origen?
  var originVerification = originResponse.body.length;
  if (!originVerification) {
    var message = `Cuenta origen no existe`;
    return response.status(400).send({ message }).end();
  }
  // ¿El Saldo es Suficiente?
  var originBalance = originResponse.body[0].cuentas[0].saldo;
  if (originBalance < amount) {
    var message = `Saldo insuficiente`;
    return response.status(400).send({ message }).end();
  }
  // CUENTA DESTINO
  try {
    var destinyResponse = await accountsModel.getBalance(destino);
  } catch (error) {
    return response.status(500).send().end(); // error del servidor
  }
  // ¿Existe Cuenta Destino?
  var destinyVerification = destinyResponse.body.length;
  if (!destinyVerification) {
    var message = `Cuenta destino no existe`;
    return response.status(400).send({ message }).end();
  }
  var destinyBalance = destinyResponse.body[0].cuentas[0].saldo;
  /*  */
  /* REALIZA OPERACIONES */
  /*  */
  var newOriginBalance = originBalance - amount;
  var newDestinyBalance = destinyBalance + amount;
  try {
    await accountsModel.updateBalance(origen, newOriginBalance);
  } catch (error) {
    return response.status(500).send().end(); // error del servidor
  }
  try {
    await accountsModel.updateBalance(destino, newDestinyBalance);
  } catch (error) {
    return response.status(500).send().end(); // error del servidor
  }
  /*  */
  /* REGISTRAR MOVIMIENTOS */
  /*  */
  var originMovement = {
    nombre,
    tipo,
    origen,
    destino,
    importe: amount,
    efecto: 'cargo',
    fecha: new Date().getTime(),
    idmovimiento: helpers.getRandomID(),
  };
  var destinyMovement = {
    nombre,
    tipo,
    origen,
    destino,
    importe: amount,
    efecto: 'abono',
    fecha: new Date().getTime(),
    idmovimiento: helpers.getRandomID(),
  };
  try {
    await movementsModel.post(origen, originMovement);
  } catch (error) {
    return response.status(500).send().end(); // error del servidor
  }
  try {
    await movementsModel.post(destino, destinyMovement);
  } catch (error) {
    return response.status(500).send().end(); // error del servidor
  }
  var message = `Operación Exitosa`;
  return response.status(201).send({ message }).end();
};

exports.get = async (request, response) => {
  var { idcliente } = request;
  try {
    var result = await movementsModel.get(idcliente);
  } catch (error) {
    return response.status(500).send().end(); // error del servidor
  }
  var movements = result.body[0].movimientos;
  return response.status(200).send(movements).end();
};
