var catalogoMovimientosModel = require('../../models/movementscatalog/movementsCatalogModel');

exports.get = async (request, response) => {
  idcliente = request.idcliente;
  try {
    var result = await catalogoMovimientosModel.get(idcliente);
  } catch (error) {
    return response.status(500).send().end(); // error del servidor
  }
  var movements = result.body;
  movements.forEach((element) => delete element._id);
  return response.status(200).send(movements).end();
};
