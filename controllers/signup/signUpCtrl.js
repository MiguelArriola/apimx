var helpers = require('../../helpers/helpers');
var customersModel = require('../../models/customers/customersModel');
var accountsModel = require('../../models/accounts/accountsModel');

exports.post = async (request, response) => {
  var customer = request.body;
  // ¿Email ya existe?
  var email = customer.email;
  try {
    var emailVerification = await customersModel.verifyEmail(email);
  } catch (error) {
    return response.status(500).send().end(); // error del servidor
  }
  if (emailVerification.body) {
    var field = `email`;
    var value = customer[field];
    var message = `${field} ${value} ya existe`;
    return response.status(400).send({ field, value, message }).end();
  }
  // ¿Cuenta ya existe?
  var account = customer.cuenta;
  try {
    var accountVerification = await accountsModel.verifyAccount(account);
  } catch (error) {
    return response.status(500).send().end(); // error del servidor
  }
  if (accountVerification.body) {
    var field = `cuenta`;
    var value = customer[field];
    var message = `${field} ${value} ya existe`;
    return response.status(400).send({ field, value, message }).end();
  }
  var data = {};
  var idcliente = helpers.getRandomID();
  data.idcliente = idcliente;
  data.nombre = customer.nombre;
  data.paterno = customer.paterno;
  data.email = customer.email;
  var password = helpers.getHash(customer.password, 10);
  data.password = password;
  data.cuentas = [{}];
  data.cuentas[0].idcuenta = customer.cuenta;
  data.cuentas[0].tipo = customer.tipo;
  data.cuentas[0].descripcion = customer.descripcion;
  data.cuentas[0].saldo = parseFloat(customer.saldo);
  data.movimientos = [];
  try {
    await customersModel.post(data);
  } catch (error) {
    return response.status(500).send().end(); // error del servidor
  }
  var message = `Se registró correctamente`;
  return response.status(201).send({ message }).end();
};
