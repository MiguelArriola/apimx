var helpers = require('../helpers/helpers');

module.exports = function (request, response, next) {
  var accessToken = request.headers.authorization;
  var message = `No ha iniciado sesión`;
  if (!accessToken) return response.status(401).send({ message }).end(); // falta accessToken
  var payload;
  try {
    payload = helpers.verifyJWT(accessToken);
  } catch (error) {
    return response.status(401).send({ message }).end(); // accessToken invalido
  }
  var idcliente = payload.idcliente;
  request.idcliente = idcliente;
  next();
};
