var requestjson = require('request-json');
var config = require('../../config/config');

var host = config.host;
var apiKey = config.apiKey;

exports.getURL = (collection, ...parameters) => {
  if (parameters) {
    return `${host}${collection}?apiKey=${apiKey}&${parameters.join('&')}`;
  }
  return `${host}${collection}?apiKey=${apiKey}`;
};

exports.getClient = (collection, ...parameters) => {
  var url = this.getURL(collection, ...parameters);
  var client = requestjson.createClient(url);
  return client;
};
