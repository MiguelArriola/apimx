var db = require('../_db/_db');

exports.verifyEmail = async (email) => {
  var collection = `Clientes`;
  var query = `q={'email': '${email}'}`;
  var count = `c=true`;
  var client = db.getClient(collection, query, count);
  var result = await client.get('');
  return result;
};

exports.getCredentials = async (email) => {
  var collection = `Clientes`;
  var query = `q={"email":"${email}"}`;
  var filter = `f={"idcliente":1,"password":1}`;
  var client = db.getClient(collection, query, filter);
  var result = await client.get('');
  return result;
};

exports.getName = async (idcliente) => {
  var collection = `Clientes`;
  var query = `q={"idcliente":'${idcliente}'}`;
  var filter = `f={"nombre":1,"paterno":1}`;
  var client = db.getClient(collection, query, filter);
  var result = await client.get('');
  return result;
};

exports.post = async (data) => {
  var collection = `Clientes`;
  var client = db.getClient(collection);
  var result = await client.post('', data);
  return result;
};
