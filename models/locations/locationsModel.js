exports.getBranches = () => {
  var data = require('./locationBranches.json');
  var branches = data.data[0].Brand[0].Branch;
  return branches;
};

exports.getATMs = () => {
  var data = require('./locationATMs.json');
  var ATMs = data.data[0].Brand[0].ATM;
  return ATMs;
};
