var db = require('../_db/_db');

exports.post = async (account, movement) => {
  var collection = `Clientes`;
  var query = `q={'cuentas': { $elemMatch: { 'idcuenta': '${account}'} } }`;
  var client = db.getClient(collection, query);
  var data = { $push: { movimientos: movement } };
  var result = await client.put('', data);
  return result;
};

exports.get = async (idcliente) => {
  var client = db.getClient(
    `Clientes`,
    `q={"idcliente":"${idcliente}"}`,
    `f={"movimientos":1}`
  );
  var result = await client.get('');
  return result;
};
