var express = require('express');
var path = require('path');
var bodyparser = require('body-parser');

var app = express();

var port = process.env.PORT || 3000;
app.listen(port);
console.log('FinCore RESTful API server started on: ' + port);

app.use(bodyparser.json());

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  );
  next();
});

app.all('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});

/* API */

var authorize = require('./middlewares/authorize');

var signUpCtrl = require('./controllers/signup/signUpCtrl');
var signInCtrl = require('./controllers/signin/signInCtrl');
var customersCtrl = require('./controllers/customers/customersCtrl');
var accountsCtrl = require('./controllers/accounts/accountsCtrl');
var movementsCatalogCtrl = require('./controllers/movementscatalog/movementsCatalogCtrl');
var movementsCtrl = require('./controllers/movements/movementsCtrl');
var locationsCtrl = require('./controllers/locations/locationsCtrl');

app.post('/Signup', signUpCtrl.post);
app.post('/Signin', signInCtrl.post);
app.post('/Customers/Movements', authorize, movementsCtrl.post);

app.get('/Customers/Name', authorize, customersCtrl.getName);
app.get('/Customers/Name', authorize, customersCtrl.getName);
app.get('/Customers/Accounts', authorize, accountsCtrl.get);
app.get('/MovementsCatalog', authorize, movementsCatalogCtrl.get);
app.get('/Customers/Movements', authorize, movementsCtrl.get);
app.get('/Locations/Branches', authorize, locationsCtrl.getBranches);
app.get('/Locations/ATMs', authorize, locationsCtrl.getATMs);
